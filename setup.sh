#!/bin/bash -ex

echo "Creating tmux config file"
#echo "source-file ~/.config/dot-files/.tmux.conf" > ~/.tmux.conf

echo "Creating neovim config file"
#mkdir -p ~/.config/nvim
#echo "source ~/.config/dot-files/init.vim" > ~/.config/nvim/init.vim

echo "Setting up neovim plugin manager"
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "Setting up base16 shell"
#git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell

cat >> ~/.bashrc << 'EOF'
# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"
EOF
