set number
set nowrap
set incsearch
set nohlsearch

"===================================
" SPACES AND TABS
"===================================
set tabstop=4 " number of spaces per TAB.
set shiftwidth=4 " number of spaces used when indenting text.
set softtabstop=0 " number o spaces inserted when inserting TAB.
set expandtab       " always convert tabs to spaces.
set smarttab


set splitright
set splitbelow

" Plugins
call plug#begin('~/.local/share/nvim/plugged')

Plug 'chriskempson/base16-vim'
Plug 'jeetsukumaran/vim-buffergator'
Plug 'christoomey/vim-tmux-navigator'
Plug 'bling/vim-airline'
Plug 'edkolev/tmuxline.vim'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-dispatch'
Plug 'bkad/CamelCaseMotion'
Plug 'rhysd/vim-clang-format'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'bronson/vim-trailing-whitespace'
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-unimpaired'

call plug#end()

"===================================
" BASE16 COLORSCHEME
"===================================
set background=dark
let base16colorspace=256  " Access colors present in 256 colorspace
colorscheme base16-circus

"===================================
" VIM-AIRLINE PLUGIN
"===================================
let g:airline#extensions#tabline#enabled = 1 "Enable the list of buffers.
let g:airline#extensions#tabline#fnamemode = ':t' "Show just the filename.

"===================================
" VIM-AIRLINE THEME
"===================================
let g:airline_theme='base16'

"===================================
" BUFFERGATOR PLUGIN
"===================================
"Buffergator config from
"https://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/
" Use the right side of the screen
let g:buffergator_viewport_split_policy = 'L'

" I want my own keymappings...
let g:buffergator_suppress_keymaps = 1

" Go to the previous buffer open
nmap <leader>jj :BuffergatorMruCyclePrev<cr>

" Go to the next buffer open
nmap <leader>kk :BuffergatorMruCycleNext<cr>

" View the entire list of buffers open
nmap <leader>bl :BuffergatorOpen<cr>

" Shared bindings from Solution #1 from earlier
nmap <leader>T :enew<cr>

"===================================
" CAMEL CASE MOTION
"===================================
call camelcasemotion#CreateMotionMappings(',')

"===================================
" CLIPBOARD
"===================================
set clipboard=unnamedplus " Use the system clipboard.

"===================================
" CLANG FORMAT
"===================================
vmap <leader>f :ClangFormat<cr>

"===================================
" MAKE BINDINGS
"===================================
set makeprg=./build.sh
nmap <F6> :Make<cr>

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
